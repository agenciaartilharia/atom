<?
//$allowSession = "nao";
require_once("lib/configs.php");
require_once("multi_idioma_request.php");
require_once("connection_mssql.php");
//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
$dbmssql = new Conexao();

// setar sess�o com nome da p�gina para ser usada no controle de acesso
//$_SESSION["care-br"]["submodulo_pagina"] = "repair_center.php";

// request
$acao = $_REQUEST["acao"];

// busca da grid
if ($acao == "grid"){
	$grid_busca = utf8_decode($_REQUEST["grid_busca"]);

	$query = "SELECT REPAIR_CENTER.ID,REPAIR_CENTER.SAP_CODE,REPAIR_CENTER.REPAIR_CENTER_NAME,REPAIR_CENTER.INITIAL_BILLING_PERIOD,REPAIR_CENTER.FINAL_BILLING_PERIOD,REPAIR_CENTER.TAT,REPAIR_CENTER.DEADLINE_PAYMENT,REPAIR_CENTER.METHOD_PAYMENT,REPAIR_CENTER.APPROVAL_INVOICE,REPAIR_CENTER.BAD_USER,REPAIR_CENTER.VARIABLE_FORECAST,REPAIR_CENTER.WORKING_DAYS,REPAIR_CENTER.CONTRACT,REPAIR_CENTER.WARRANTY_REPAIR_DEADLINE,REPAIR_CENTER.CUSTOMER_CARE,REPAIR_CENTER.CALCULATOR_FINE,
		BILLING.BILLING_DESCRIPTION
		 FROM REPAIR_CENTER
		 LEFT JOIN BILLING ON BILLING.ID = REPAIR_CENTER.TYPE_PAYMENT";
	if($grid_busca)
		$query .= " where NAME LIKE '%$grid_busca%' ";

	$query .= ' order by ID ';
	$qtd = 40;
	$pagina = $_REQUEST['pagina'];
	if(!$pagina) $pagina = 1;
	$offset = ($pagina -1) * $qtd;
	$data = $dbmssql->get($query, $offset, $qtd);

	// busca total
	$query = "select count(*) as t from REPAIR_CENTER";
	if($grid_busca)
		$query .= " where REPAIR_CENTER LIKE '%$grid_busca%' ";
		$total = $dbmssql->get($query)[0]['t'];
		$pagina_total = ceil($total / $qtd);
		if($offset == 0){
			$offset = 1;
			$qtd--;
		}
		if ($offset > $total)
			$offset = $total;
		$total_registro_pagina = $offset + $qtd;
		if($total_registro_pagina > $total)
			$total_registro_pagina = $total;
	?>
	<div id="pageContent">
		<input type="hidden" name="filtro_sql" id="filtro_sql" value="<?=$query?>" />


		<a href="repair_center_edicao.php?acao=add" title="Novo " alt="Novo "><i class="icon-new"></i>Novo </a>
		<a onclick="exportaBase()" href="#" title="Exportar " alt="Exportar "><i class="icon-file-excel"></i>Exportar </a>
		<a href="uploader.php?tipo=REPAIR_CENTER" title="Importar Planilha"><i class="icon-upload-3"></i>Importar Planilha CSV </a>

		<!--a onclick="dialogFiltro()" href="#" title="Filtro " alt="Filtro "><i class="icon-filter"></i>Filtro </a-->

		<div class="input-control text">
			<input type="text" id="grid_busca" name="grid_busca" placeholder="<?=fct_get_var('global.php', 'var_grid_pesquisa', $_SESSION["care-br"]["idioma_id"])?>" />
			<a href="javascript: void(0);" onclick="loadGrid(); return false;"><button class="btn-search"></button></a>
		</div>
		<br><?="$offset - $total_registro_pagina de $total"?>
		<table class="striped bordered hovered">
			<thead>
				<tr>
					<th align='center'>SAP Code</th>
					<th>Provider Name</th>
					<th>Initial Billing Period</th>
					<th>Final Billing Period</th>
					<th>TAT</th>
					<th>Deadline Payment</th>
					<th>Method Payment</th>
					<th>Type Payment</th>
					<th>Approval Invoice</th>
					<th>Bad User</th>
					<th>Variable Forecast</th>
					<th>Workings Days</th>
					<th>Contract</th>
					<th>Warranty Repair	Deadline</th>
					<th>Customer Care</th>
					<th>Calculator Fine</th>
					<th class="text-center"><?=fct_get_var('global.php', 'var_grid_acao', $_SESSION["care-br"]["idioma_id"])?></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($data as $row){ ?>
				<tr>
					<td><?=$row['SAP_CODE']?></td>
					<td><?=$row['REPAIR_CENTER_NAME']?></td>
					<td><?=$row['INITIAL_BILLING_PERIOD']?></td>
					<td><?=$row['FINAL_BILLING_PERIOD']?></td>
					<td><?=$row['TAT']?></td>
					<td><?=$row['DEADLINE_PAYMENT']?></td>
					<td><?=$row['METHOD_PAYMENT']?></td>
					<td><?=$row['BILLING_DESCRIPTION']?></td>
					<td><?=$row['APPROVAL_INVOICE']?></td>
					<td><?=$row['BAD_USER']?></td>
					<td><?=$row['VARIABLE_FORECAST']?></td>
					<td><?=$row['WORKING_DAYS']?></td>
					<td><?=$row['CONTRACT']?></td>
					<td><?=$row['WARRANTY_REPAIR_DEADLINE']?></td>
					<td><?=$row['CUSTOMER_CARE']?></td>
					<td><?=$row['CALCULATOR_FINE']?></td>
					<td width="300" align="center">
						<a href="repair_center_edicao.php?acao=updt&id=<?=$row['ID']?>" title="Alterar" alt="Alterar"><i class="icon-pencil"></i></a>
						<a href="javascript:;" onclick="excluirCadastro(<?=$row['ID']?>)" title="Excluir Registro" alt="Excluir"><i class="icon-remove"></i></a>
					</td>
				</tr>
			<?php } ?>
			</tbody>
			<tfoot></tfoot>
		</table>

		<?
		// ---------
		// pagina��o
		// ---------
		if ($total > 1){
			?>
			<div id="pageDiv"></div>
			<script type="text/javascript" src="js/modern/pagelist.js"></script>
			<script type="text/javascript">
				$(document).ready(function(){
					var page = $("#pageDiv").pagelist();
					page.total = <?=$pagina_total?>;
					page.current = <?=$pagina?>;

					// vari�veis de filtro e busca
					var grid_busca = $("#grid_busca").val();

					page.url = "repair_center_edicao.php?pagina={page}&acao=grid&grid_busca=" + grid_busca;
					page.ajax = "#pageContent";
					page.create();
				});
			</script>
			<?
		}
		// -------------
		// fim pagina��o
		// -------------
		?>
	</div>
	<?
}

// ---------------------
// inclus�o ou altera��o
// ---------------------
if (($acao == "add") || ($acao == "updt")){

	$id = $_REQUEST['id'];
	if($id > 0){
		$res = $dbmssql->get("SELECT REPAIR_CENTER.ID,REPAIR_CENTER.SAP_CODE,REPAIR_CENTER.REPAIR_CENTER_NAME,REPAIR_CENTER.INITIAL_BILLING_PERIOD,REPAIR_CENTER.FINAL_BILLING_PERIOD,REPAIR_CENTER.TAT,REPAIR_CENTER.DEADLINE_PAYMENT,REPAIR_CENTER.METHOD_PAYMENT,REPAIR_CENTER.APPROVAL_INVOICE,REPAIR_CENTER.BAD_USER,REPAIR_CENTER.VARIABLE_FORECAST,REPAIR_CENTER.WORKING_DAYS,REPAIR_CENTER.CONTRACT,REPAIR_CENTER.WARRANTY_REPAIR_DEADLINE,REPAIR_CENTER.CUSTOMER_CARE,REPAIR_CENTER.CALCULATOR_FINE,
			BILLING.BILLING_DESCRIPTION
			 FROM REPAIR_CENTER
			 LEFT JOIN BILLING ON BILLING.ID = REPAIR_CENTER.TYPE_PAYMENT where REPAIR_CENTER.ID = $id");
		if(!$res[0])
			die("Country não encontrada");

		$data = $res[0];
	}

	include("header.php");
	?>

	<!-- validator -->
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="js/funcoes_jquery.js"></script>
	<!-- fim validator -->

	<script type="text/javascript">
		$(document).ready(function() {
			// validar dados
			$('#frm_cadastro').validate({
				rules:{
							SAP_CODE: "require",
							REPAIR_CENTER_NAME: "require",
							INITIAL_BILLING_PERIOD: "require",
							FINAL_BILLING_PERIOD: "require",
							TAT: "require",
							DEADLINE_PAYMENT: "require",
							METHOD_PAYMENT: "require",
							TYPE_PAYMENT: "require",
							APPROVAL_INVOICE: "require",
							BAD_USER: "require",
							VARIABLE_FORECAST: "require",
							WORKING_DAYS: "require",
							CONTRACT: "require",
							WARRANTY_REPAIR_DEADLINE: "require",
							CUSTOMER_CARE: "require",
							CALCULATOR_FINE: "require",
				},
				messages:{
					SAP_CODE:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
					REPAIR_CENTER_NAME:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
					INITIAL_BILLING_PERIOD:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
					FINAL_BILLING_PERIOD:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
					TAT:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
					DEADLINE_PAYMENT:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
					METHOD_PAYMENT:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
					TYPE_PAYMENT:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
					APPROVAL_INVOICE:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
					BAD_USER:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
					VARIABLE_FORECAST:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
					WORKING_DAYS:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
					CONTRACT:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
					WARRANTY_REPAIR_DEADLINE:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
					CUSTOMER_CARE:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
					CALCULATOR_FINE:  "<?=fct_get_var('global.php', 'var_campo_obrigatorio', $_SESSION["care-br"]["idioma_id"])?>",
				},
				submitHandler: function(form) {
					if (confirm("<?=fct_get_var('global.php', 'var_confirma_cadastro', $_SESSION["care-br"]["idioma_id"])?>")){
						$.ajax({
						  type: "POST",
						  url: 'repair_center_acao.php',
						  data: $("#frm_cadastro").serialize(),
						  async: false,
						  success: function(data) {
							window.location.href="repair_center.php";
						  }
						});
					}
					// n�o submeter formul�rio, gravar via AJAX
					return false;
				}
			});
		});
	</script>

    <style>
        .span6.campos-form {
            margin-right: 20px !important;
        }
        form#frm_cadastro {
            margin-top: 20px !important;
        }
    </style>

	<div class="page secondary">
        <div class="page-header">
            <div class="page-header-content titulo-internas">
      			<h1>
                    <a href="repair_center.php" class="back-button big page-back" title="<?=fct_get_var('global.php', 'var_retornar', $_SESSION["care-br"]["idioma_id"])?>" alt="<?=fct_get_var('global.php', 'var_retornar', $_SESSION["care-br"]["idioma_id"])?>"></a>
                    Provider<small><?php echo utf8_decode("Edição");?></small>
                </h1>
            </div>
        </div>

        <div class="page-region">
            <div class="form-pags">
				<div class="grid">
					<form id="frm_cadastro" name="frm_cadastro" method="post">
	<input type="hidden" name="acao" id="acao" value="<?=$acao?>" />
	<input type="hidden" name="id" id="id" value="<?=$id?>" />
	<div class="row">
		<div class="span6 campos-form">
			<label>SAP CODE</label>
			<div class="input-control text">
				<input type="text" id="SAP_CODE" name="SAP_CODE" value="<?=trim($data["SAP_CODE"])?>">
			</div>
			</div>
			<div class="span6 campos-form">
				<label>Provider Name</label>
				<div class="input-control text">
					<input type="text" id="REPAIR_CENTER_NAME" name="REPAIR_CENTER_NAME" value="<?=trim($data["REPAIR_CENTER_NAME"])?>">
				</div>
				</div>

				<div class="span6 campos-form">
						<label>Initial Billing Period</label>
						<div class="input-control text">
							<input type="text" id="INITIAL_BILLING_PERIOD" name="INITIAL_BILLING_PERIOD" value="<?=trim($data["INITIAL_BILLING_PERIOD"])?>">
						</div>
					</div>

					<div class="span6 campos-form">
							<label>Final Billing Period</label>
							<div class="input-control text">
								<input type="text" id="FINAL_BILLING_PERIOD" name="FINAL_BILLING_PERIOD" value="<?=trim($data["FINAL_BILLING_PERIOD"])?>">
							</div>
						</div>

						<div class="span6 campos-form">
								<label>TAT</label>
								<div class="input-control text">
									<input type="text" id="TAT" name="TAT" value="<?=trim($data["TAT"])?>">
								</div>
							</div>

							<div class="span6 campos-form">
									<label>Deadline Payment</label>
									<div class="input-control text">
										<input type="text" id="DEADLINE_PAYMENT" name="DEADLINE_PAYMENT" value="<?=trim($data["DEADLINE_PAYMENT"])?>">
									</div>
								</div>

								<div class="span6 campos-form">
										<label>Method Payment</label>
										<div class="input-control text">
											<input type="text" id="METHOD_PAYMENT" name="METHOD_PAYMENT" value="<?=trim($data["METHOD_PAYMENT"])?>">
										</div>
									</div>

									<div class="span6 campos-form">
											<label>Type Payment</label>
											<div class="input-control text">
												<input type="text" id="TYPE_PAYMENT" name="TYPE_PAYMENT" value="<?=trim($data["BILLING_DESCRIPTION"])?>">
											</div>
										</div>

										<div class="span6 campos-form">
												<label>Approval Invoice</label>
												<div class="input-control text">
													<input type="text" id="APPROVAL_INVOICE" name="APPROVAL_INVOICE" value="<?=trim($data["APPROVAL_INVOICE"])?>">
												</div>
											</div>

											<div class="span6 campos-form">
													<label>Bad User</label>
													<div class="input-control text">
														<input type="text" id="BAD_USER" name="BAD_USER" value="<?=trim($data["BAD_USER"])?>">
													</div>
												</div>

												<div class="span6 campos-form">
														<label>Variable Forecast</label>
														<div class="input-control text">
															<input type="text" id="VARIABLE_FORECAST" name="VARIABLE_FORECAST" value="<?=trim($data["VARIABLE_FORECAST"])?>">
														</div>
													</div>

													<div class="span6 campos-form">
															<label>Workings Days</label>
															<div class="input-control text">
																<input type="text" id="WORKING_DAYS" name="WORKING_DAYS" value="<?=trim($data["WORKING_DAYS"])?>">
															</div>
														</div>

														<div class="span6 campos-form">
																<label>Contract</label>
																<div class="input-control text">
																	<input type="text" id="CONTRACT" name="CONTRACT" value="<?=trim($data["CONTRACT"])?>">
																</div>
															</div>

															<div class="span6 campos-form">
																	<label>Warranty Repair	Deadline</label>
																	<div class="input-control text">
																		<input type="text" id="WARRANTY_REPAIR_DEADLINE" name="WARRANTY_REPAIR_DEADLINE" value="<?=trim($data["WARRANTY_REPAIR_DEADLINE"])?>">
																	</div>
																</div>
																<div class="span6 campos-form">
																		<label>Customer Care</label>
																		<div class="input-control text">
																			<input type="text" id="CUSTOMER_CARE" name="CUSTOMER_CARE" value="<?=trim($data["CUSTOMER_CARE"])?>">
																		</div>
																	</div>
																	<div class="span6 campos-form">
																			<label>Calculator Fine</label>
																			<div class="input-control text">
																				<input type="text" id="CALCULATOR_FINE" name="CALCULATOR_FINE" value="<?=trim($data["CALCULATOR_FINE"])?>">
																			</div>
																		</div>



				</div>
				<div class="row">
					<div class="btn-salvar">
						<input type="reset" value="<?=fct_get_var('global.php', 'var_botao_redefinir', $_SESSION["care-br"]["idioma_id"])?>"><input type="submit" value="<?=fct_get_var('global.php', 'var_botao_confirmar', $_SESSION["care-br"]["idioma_id"])?>">
					</div>
				</div>
			</form>
				</div>
			</div>
        </div>
    </div>
		<script>
		$("#TYPE_PAYMENT").autocomplete({
						source: function( request, response ) {
							$.ajax({
								url: "repair_center_acao.php?acao=pesquisa_type_pyment_autocomplete",
								dataType: "json",
								data: {q: request.term},
								success: function( data ) {
									response(data);
								}
							});
						},
						minLength: 3,
						select: function( event, ui ) {

							var id = ui.item.label.split('-');
							// console.log(id)

							$( "#TYPE_PAYMENT").val($.trim(id[0]));
							// $( "#det_prod_cProd").val($.trim(id[0]));
							// $( "#produto_id").val($.trim(id[0]));
							return false;

						},
						open: function() {
							$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
						},
						close: function() {
							$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
						}
				});
		</script>


	<?
	include("footer.php");
}

// -------------
// dialog filtro
// -------------
if ($acao == "dialog_filtro"){
	?>
	<div class="row">
		<div class="input-control select span5">
			<h3><?=fct_get_var('global.php', 'var_ativo', $_SESSION["care-br"]["idioma_id"])?></h3>
			<select id="fornecedor_ativo">
				<option value=""><?=fct_get_var('global.php', 'var_selecione', $_SESSION["care-br"]["idioma_id"])?></option>
				<option value="S" <? if ($filtro_fornecedor_ativo == "S") echo "selected"; ?> ><?=fct_get_var('global.php', 'var_sim', $_SESSION["care-br"]["idioma_id"])?></option>
				<option value="N" <? if ($filtro_fornecedor_ativo == "N") echo "selected"; ?> ><?=fct_get_var('global.php', 'var_nao', $_SESSION["care-br"]["idioma_id"])?></option>
			</select>
		</div>
	</div>
	<?
}
?>
